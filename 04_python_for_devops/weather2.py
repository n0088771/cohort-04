#! /usr/bin/env python3
import requests

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
     """
    response = requests.get('https://ipvigilante.com/')

    data = response.json()
    lat = data['data']['latitude']
    lon = data['data']['longitude']



    return lat, lon

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    SECRET_KEY="d21b17405d692b8977dd9098c59754eb"
    url = 'https://api.darksky.net/forecast/{key}/{lat},{long}'.format(
    key=SECRET_KEY, lat=latitude, long=longitude)
    response1 = requests.get(url)
    response1_json = response1.json()
    temper = response1_json['currently']['temperature']
    
    return temper

def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print(f"\nToday's current temperature is: {temp}\n")

if __name__ == "__main__":
    latitude, longitude = get_location()
    temp = get_temperature(longitude, latitude)
    print_forecast(temp)