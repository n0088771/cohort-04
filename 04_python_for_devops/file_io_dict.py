
def dict_check(dict, key):
    if key in dict.keys():
        return True
    else:
        return False

word_dict = {}
poem_list = []
alpha_numeric = ""

with open('/home/alyons/code/cohort-04/04_python_for_devops/poem.txt', 'r') as f1:
    poem_var = f1.read()

poem_list = poem_var.split()

for word in poem_list:
    alpha_numeric = ""
    for char in word:
        if char.isalnum():
            alpha_numeric += char
  
    temp_var = 0
    if dict_check(word_dict, alpha_numeric.lower()):
        temp_var = word_dict.get(alpha_numeric.lower())
        temp_var += 1
        word_dict[alpha_numeric.lower()] = temp_var
    else:
        word_dict[alpha_numeric.lower()] = 1

# sorted_d = sorted(word_dict.items(), key=lambda x: x[1])

for key_var, value_var in word_dict.items():
    print("{0:<15}{1:<15}".format(key_var, value_var))

word_dict

# inp_dict = { 'a':3,'ab':2,'abc':1,'abcd':0 }
# print("Dictionary: ", inp_dict)
# sort_dict= dict(sorted(inp_dict.items(), key=lambda item: item[1])) 
# print("Sorted Dictionary by value: ", sort_dict)