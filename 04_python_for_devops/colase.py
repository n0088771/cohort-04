
def conjecture(integer_var):
    """Used to test the Collatz Conjecture"""
    if integer_var > 1:
        if integer_var % 2 == 0:
            return integer_var // 2
        else:
            return (integer_var * 3) + 1
    return integer_var



conjecture_var = int(input("Please enter a digit for processing: "))

while int(conjecture_var) != 1:
    conjecture_var = conjecture(int(conjecture_var))
    print(conjecture_var)
