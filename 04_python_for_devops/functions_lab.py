

def calculate(num1, num2, oper):
    """ Used to perform calucations on numbers."""
    if oper.upper() == "A":
        output_var = (int(num1) + int(num2))
    elif oper.upper() == "S":
        output_var = (int(num1) - int(num2))
    elif oper.upper() == "M":
        output_var = (int(num1) * int(num2))
    elif oper.upper() == "D":
        output_var = (int(num1) // int(num2))
    else:
        output_var = False
    
    return output_var

def sumdigits(num1):
    """Used to sum all the digits that make up a number."""
    output_var = 0
    for num in num1:
        output_var += int(num)
    
    return output_var

def conjecture(integer_var):
    """Used to test the Collatz Conjecture"""
    if integer_var > 1:
        if integer_var % 2 == 0:
            return integer_var // 2
        else:
            return (integer_var * 3) + 1
    return 0


input_var_num1 = ""
input_var_num2 = ""
input_var_operator = ""
output_print = ""
init_sumdigits = ""
final_sumdigits = ""
process_var = 0

input_var_num1 = input("\n Enter the first number: ")
input_var_num2 = input("\n Enter the second number: ")
input_var_operator = input("Enter a desired function \n \n"
                            "\t (A)dd: \n"
                            "\t (S)ubtract: \n"
                            "\t (M)ultiple: \n"
                            "\t (D)ivid: \n"
                            "\t (Q)uit: \n \n"
                             "Enter your selection:")

output_print = calculate(input_var_num1, input_var_num2, input_var_operator)

print("\n" + str(output_print) + "\n")

input_var_sumdigits = input("\nPlease enter a digit for double sum processing: ")

init_sumdigits = sumdigits(str(input_var_sumdigits))
final_sumdigits = sumdigits(str(init_sumdigits))

print(f"\nFirst SUM was {init_sumdigits} and second SUM was {final_sumdigits} \n")

conjecture_var = int(input("Please enter a digit for processing: "))

while int(conjecture_var) != 1:
    conjecture_var = conjecture(int(conjecture_var))
    print(conjecture_var)

