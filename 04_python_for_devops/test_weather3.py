import unittest
import weather2


def test_get_location():
    latitude, longitude = weather2.get_location()
    expected1 = '39.04810'
#        expected1 = '49.04810'
    expected2 = '-77.47280'
    assert latitude == expected1
    assert longitude == expected2

def test_get_temperature():
    latitude, longitude = weather2.get_location()
    returned = weather2.get_temperature(longitude, latitude)
    expected = 77
    if returned != expected:
        pass
    else:
        assert returned== expected
#        self.assertEqual(returned, expected)



