

def sumdigits(num1):
    """Used to sum all the digits that make up a number."""
    process_var = 0
    while process_var > 9 or process_var == 0:
        for num in num1:
            process_var += int(num)
        
    return output_var


input_var_sumdigits = input("\nPlease enter a digit for double sum processing: ")

init_sumdigits = sumdigits(str(input_var_sumdigits))
final_sumdigits = sumdigits(str(init_sumdigits))

print(f"\nFirst SUM was {init_sumdigits} and second SUM was {final_sumdigits} \n")