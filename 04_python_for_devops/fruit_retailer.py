import sys

def add_produce(itemvar, cost, promo, promo_qty, promo_price):
     if itemvar not in produce.keys():
         produce[itemvar] = {}
    
     produce[itemvar]['price'] = cost
     produce[itemvar]['disc'] = promo
     produce[itemvar]['disc_qty'] = promo_qty
     produce[itemvar]['disc_price'] = promo_price
     return True

produce = {}
add_produce('Apple', .99, True, 3, 2.00)
add_produce('Banana', 2.99, False, 0, 0)
add_produce('Orange', 1.15, False, 0, 3.45)
add_produce('Grape', 1.99, False, 0, 0)
add_produce('Mango', 2.50, False, 0, 0)

print("\nHere are the items that you can order: \n")

for key, value in produce.items():
    # print(key, value)

    print(key, "\t{:.2f}".format(produce[key]['price']))

selection_var = input("\nEnter a selection: ")


if selection_var not in produce.keys():
    print("You did not enter a valid item. Start over")
    sys.exit()

selection_num_var = int(input("\nEnter number desired: "))


# produce['apple'] = {}
# produce['banana'] = {}
# produce['orange'] = {}
# produce['mango'] = {}
# produce['grape'] = {}
# produce['apple']['price'] = .99
# produce['apple']['disc'] = True
# produce['apple']['qty_disc'] = 3
# produce['apple']['disc_price'] = 2.00
# produce['banana']['price'] = 2.99
# produce['banana']['disc'] = False
# produce['banana']['qty_disc'] = 0
# produce['banana']['disc_price'] = 0.00
# produce['orange']['price'] = 1.15
# produce['orange']['disc'] = False
# produce['orange']['qty_disc'] = 0
# produce['orange']['disc_price'] = 0.00
# produce['mango']['price'] = 2.50
# produce['mango']['disc'] = True
# produce['mango']['qty_disc'] = 2
# produce['mango']['disc_price'] = 1.25
# produce['grape']['price'] = 1.99
# produce['grape']['disc'] = False
# produce['grape']['qty_disc'] = 0
# produce['grape']['disc_price'] = 0.00




