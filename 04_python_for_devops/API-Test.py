import requests
import json


ip_address = input("\nEnter and IP address: ")

payload = {'key1': 'value1', 'key2': 'value2'}
# r = requests.get('http://httpbin.org/get', params=payload)
r = requests.get('https://ipwhois.app/json/' + ip_address)
print('URL: ', r.url)
print('ENCODING: ', r.encoding)
print('STATUS_CODE: ', r.status_code)
data = json.loads(r.text)
print("\n\nIP address queried: \t{}".format(data['ip']))
print("Query successful: \t{}".format(data['success']))
print("Address type found: \t{}".format(data['type']))
print("Organization owner: \t{}".format(data['org']))
print("Address country: \t{}\n\n".format(data['country']))


#print(data)
# print(r.text)
