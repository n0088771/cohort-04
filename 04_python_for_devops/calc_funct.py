import math
def calculate(num1, num2, oper):
    """ Used to perform calucations on numbers."""
    if oper.upper() == "A":
        output_var = (int(num1) + int(num2))
    elif oper.upper() == "S":
        output_var = (int(num1) - int(num2))
    elif oper.upper() == "M":
        output_var = (int(num1) * int(num2))
    elif oper.upper() == "D":
        try:
            output_var = (int(num1) / int(num2))
        except ZeroDivisionError:
            print("\nYou cannot divide by zero! \n\nTry again!\n")
            exit()
    elif oper.upper() == "L":
        try:
            output_var = math.log(int(num1)) / math.log(int(num2))
        except Exception as whoa:
            print("You had an exception of type", whoa, type(whoa))
            exit()

    else:
        output_var = False
    
    return output_var