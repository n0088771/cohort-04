import unittest
import weather2

class TestWeather2(unittest.TestCase):

    def test_get_location(self):
        latitude, longitude = weather2.get_location()
        expected1 = '39.04810'
#        expected1 = '49.04810'
        expected2 = '-77.47280'
        self.assertEqual(latitude, expected1)
        self.assertEqual(longitude, expected2)

    def test_get_temperature(self):
        latitude, longitude = weather2.get_location()
        returned = weather2.get_temperature(longitude, latitude)
        expected = 77
        if returned != expected:
            pass
        else:
            self.assertEqual(returned, expected)
#        self.assertEqual(returned, expected)



