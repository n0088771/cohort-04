import math
import sys

def calculate(num1, num2, oper):
    """ Used to perform calucations on numbers."""
    if oper.upper() == "A":
        output_var = (int(num1) + int(num2))
    elif oper.upper() == "S":
        output_var = (int(num1) - int(num2))
    elif oper.upper() == "M":
        output_var = (int(num1) * int(num2))
    elif oper.upper() == "D":
        try:
            output_var = (int(num1) / int(num2))
        except ZeroDivisionError:
            print("\nYou cannot divide by zero! \n\nTry again!\n")
            exit()
    elif oper.upper() == "L":
        try:
            output_var = math.log(int(num1)) / math.log(int(num2))
        except Exception as whoa:
            print("You had an exception of type", whoa, type(whoa))
            exit()

    else:
        output_var = False
    
    return output_var


input_var_num1 = ""
input_var_num2 = ""
input_var_operator = ""
output_print = ""
init_sumdigits = ""
final_sumdigits = ""
process_var = 0
index_var = 0

# for arg in sys.argv):
#     index_var += 1    
#     values(1)
#     number2
#     number3

input_var_num1 = sys.argv[0]
input_var_num2 = sys.argv[1]
input_var_operator = sys.argv[2]
output_print = calculate(input_var_num1, input_var_num2, input_var_operator)
print("\n" + str(output_print) + "\n")
    
    # print("arg %d is %so" % (idx, arg))


# input_var_num1 = input("\n Enter the first number: ")
# input_var_num2 = input("\n Enter the second number: ")
# input_var_operator = input("Enter a desired function \n \n"
#                             "\t (A)dd: \n"
#                             "\t (S)ubtract: \n"
#                             "\t (M)ultiple: \n"
#                             "\t (D)ivide: \n"
#                             "\t (L)og: \n"
#                             "\t (Q)uit: \n \n"
#                              "Enter your selection: ")

# output_print = calculate(input_var_num1, input_var_num2, input_var_operator)

p