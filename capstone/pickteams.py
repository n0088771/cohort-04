import random
import sys


def get_roster(filename):
    roster = []
    with open(filename) as f:
        for line in f:
            roster.append(line.strip())
    return roster


def partition(roster, num_parts):
    random.shuffle(roster)
    teams = [roster[i::num_parts] for i in range(num_parts)]
    return teams


if __name__ == '__main__':
    filename = sys.argv[1]
    parts = int(sys.argv[2])
    roster = get_roster(filename)
    teams = partition(roster, parts)
    for i, team in enumerate(teams):
        print('Team', i + 1, '\n\t' + '\n\t'.join(team))
