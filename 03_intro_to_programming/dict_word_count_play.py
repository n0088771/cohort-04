from os import system, name
import string

def screen_clear():
   if name == 'nt':
      _ = system('cls')
   # for mac and linux(here, os.name is 'posix')
   else:
      _ = system('clear')

# screen_clear()

words_dict = {}
words_list = []

with open('poem.txt') as file:
    words_list = list(file)

# input_var = input("Please enter a string of words to be analyzed: ")
# words_list = input_var.split()

for words in words_list:
    word_strings = words.split()
    for word in word_strings:
        word = word.strip()
        word = word.translate(str.maketrans('', '', string.punctuation))
        if word not in words_dict:
            words_dict[word] = 1
        else:
            words_dict_val = words_dict[word]
            words_dict_val += 1
            words_dict[word] = words_dict_val
    print(words)    


# for key, value in words_dict.items():
#     print(f"{key:15} --> {value:10}")


print(words)
