list1 = ["cars","planes","bicycle"]
list2 = ["ant","grasshpper","moth","gnat","cockroach","spider"]
list3 = ["ant","grasshpper","moth","gnat","cockroach","spider"]
list4 = ["ant","grasshpper","moth","gnat","cockroach","spider",["ant","grasshpper","moth","gnat","cockroach","spider"]]

print("The inner list length is ",len(list4[6]))
print("The 3rd value in the internal list is ", list4[6][2])

for insects in list3:
    print(insects)

for insects in list3:
    print(insects[0:3])

for insects in list3:
    print(insects[::2])

# adding to a list and then using slicer to output

input_string = ""
output_list = []
while input_string != "quit":
    input_string = input("Enter a value: ")
    if input_string != "quit":
        output_list.append(input_string)
    else:
        print("You chose to quit")

print(output_list[0::2])
print(output_list[1::2])

# Creating a list with split()

comma_separated = "eggs, milk, butter, cheese"
shopping_list = comma_separated.split(', ')
print(shopping_list)
