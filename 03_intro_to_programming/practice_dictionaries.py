example1 = {}
example2 = {'Buick':'GM','Malibu':'GM','Mustang':'Ford','300':'Chrysler'}
example2['Malibu'] = 'Chevy'
del example2['Malibu']
example2['Malibu'] = 'Chevy'
result = example2.get('Malibu')
print(example2)
print(example2['Malibu'])
if result == None:
    print("Try again!")
else:
    print(f"Malibu equals {result}")