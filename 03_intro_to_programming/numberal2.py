roman = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}

numeral = input{'Enter a roman numeral'}
total = 0
prev = 0
for char in numeral:
    value roman.get(char)
    if not value:
        print('Invalid roman numeral!")
        break
    total += value
    if prev < value:
        total += 2 * prev
    prev = value
print('Arabic equivalent: ', total)