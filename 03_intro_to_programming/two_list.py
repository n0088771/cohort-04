

list1_opt = []
list2_opt = []

while True:
    list_update = input("Would you like to update a list (y or n)? ")
    if list_update == "y":
        list_selected = input("Which list you would you like to update (list1 or list2)? ")
        if list_selected == "list1" or list_selected == "list2":
            list_action = input("Which would you like to do to the list: \n \n   Add item (a) \n   Remove item (r) \n   Sort (s) \n  \n \n Enter response: ")
            if list_action == "a":
                list_var = input("Enter what you would like to add to the list? ")
                if list_selected == "list1":
                    list1_opt.append(list_var)
                elif list_selected == "list2":
                    list2_opt.append(list_var)
            elif list_action == "r":
                list_remove_option = input("Do you want to remove from the list by value or index? ")
                if list_remove_option == "value":
                    list_var = input("Enter value you would like to remove from the list: ")
                    if list_selected == "list1":
                        if list_var in list1_opt:
                            list1_opt.remove(list_var)
                    else:
                        if list_var in list2_opt:
                            list2_opt.remove(list_var) 
                elif list_remove_option == "index":
                    list_var = input("Enter value index you would like to remove from the list: ") 
                    if list_selected == "list1":
                        del list1_opt[int(list_var)]
                    else:
                        del list2_opt[int(list_var)]   
                else:
                    print("Your response was not recognized, start over!")
                    break
            elif list_action == "s":
                list_sort_option = input("Would you like to sort the list forward or reversed?")
                if list_selected == "list1":
                    if list_sort_option == "forward":
                        list1_opt.sort()
                    elif list_sort_option == "reverse":
                        list1_opt.sort(reverse=True)
                elif list_selected == "list2":
                    if list_sort_option == "forward":
                        list2_opt.sort()
                    elif list_sort_option == "reverse":
                        list2_opt.sort(reverse=True)
                else:
                    print("Your response was not recognized, start over!")
                    break
            else:
                print("Your response was not recognized, start over!")
                break
    else:
        break


print("\nHere are the items in list1: ")
for item in list1_opt:
    print(item)

print("Here are the items in list2: ")
for item in list2_opt:
    print(item)
