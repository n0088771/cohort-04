import re
from os import system, name

# Pattern for regex
pattern = '[MDCLXVI]'

# Dictionary for conversion 
roman_dict = {'M':'1000', 'D':'500', 'C':'100', 'L':'50', 'X':'10', 'V':'5', 'I':'1'}

# Establish variables
roman_numeral_previous = 0
roman_numerals = 0
roman_numerals_combined = 0
roman_combined = 0

# Clear Screen function

def screen_clear():
   if name == 'nt':
      _ = system('cls')
   # for mac and linux(here, os.name is 'posix')
   else:
      _ = system('clear')

screen_clear()

numerals_combined = 0
input_var = input("Enter your roman numberal to convert: ")


for var_check in input_var:
    # Regex to insure characters entered are roman numerals, exit if not
    var_checked = re.match(pattern, var_check)
    if not var_checked:
        print("You must enter valid roman numerals as capital letters, exiting script!")
        exit()

# Convert each roman numberal value returned to integer
for roman_numeral in input_var:
    roman_numerals = int(roman_dict[roman_numeral])
    # Validate all numerals entered are smaller than the previous numeral to add or otherwise subtract
    if roman_numerals < roman_numeral_previous and roman_numeral_previous != 0:
        numerals_combined -= roman_numerals
    else:
        numerals_combined += roman_numerals

    roman_numeral_previous = roman_numerals

print(f"Your roman numerals total {numerals_combined}")


