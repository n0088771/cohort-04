import random
from os import system, name

def screen_clear():
   if name == 'nt':
      _ = system('cls')
   # for mac and linux(here, os.name is 'posix')
   else:
      _ = system('clear')

screen_clear()

input_var = input("Enter a word you want to use for the game: ")

print("This is a word scramble game.", end="\n\n" )

container = list(input_var)
random.shuffle(container)
#random.choice(container)
scrambled_word = ""

for var_word in container:
    scrambled_word += var_word

while True:
    print(f"Your scrambled word is: {scrambled_word}", end="\n\n" )
    input_var2 = input("Enter your guess as to what the word might be: ")
    if input_var2 == "quit":
        print(f"Sorry you chose to quit, the word was: {input_var}")
        break
    if input_var2 == input_var:
        print("You guessed the right word!!!!!!", end="\n\n" )
        break
    else:
        print("Your guessed was incorrect, try again!!!!!!", end="\n\n" )














