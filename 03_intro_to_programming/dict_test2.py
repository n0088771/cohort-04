numeral_dict = { 'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
numeral_dict
#input_var = ('Provide a roman numeral to be translated: ')
print(numeral_dict.get("M"))
numeral_dict['M'] = 1001
print(numeral_dict.get("M"))
del numeral_dict['M']
print(numeral_dict.get("M"))
numeral_dict['M'] = 1001
print(numeral_dict.get("M"))