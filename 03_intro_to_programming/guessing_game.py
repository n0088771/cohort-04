import random
number = random.randint(1, 100)
input_var = ""

def valid_digit(input_number):
  if input_number and input_number.isdigit():
      return int(input_number)  #return an integer
  else:
      return 0

while input_var != number:
    input_var = input("Enter a number between 1 - 100: ")
    input_var2 = valid_digit(input_var)    
    if int(input_var) == 0:
        print("Sorry you chose to quit, come again soon!")
        break
    else:    
        if input_var2 > 0:
            print(f"The number is {number}")
            if int(input_var) > number:
                print(f"Your guess of {input_var} is to high.")
            elif int(input_var) < number:
                print(f"Your guess of {input_var} is to low.")
            else:
                print(f"You guessed it correctly!")
                break
        else:
            print("You did not enter an integer. Start over!")
            break