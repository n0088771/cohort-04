
inputVar1 = input("Enter a string: ")
inputVar2 = input("Enter a stride: ")

counter = 0
outputVar = ""

upper_case_var = True

for inputVar in inputVar1:
    counter += 1
    if counter <= int(inputVar2) and upper_case_var == True:
        outputVar = outputVar + inputVar.upper()
    else:
        outputVar = outputVar + inputVar.lower()

    if counter == int(inputVar2):
        counter = 0
        if upper_case_var == True:
            upper_case_var = False
        else:
            upper_case_var = True


print(outputVar)