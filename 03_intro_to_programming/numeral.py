numeral_dict = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
#print(numeral_dict["M"])
input_var = ""
#input_var = input("Provide a roman numeral to be translated: ")
value = 0
last_value = 0
total = 0
first_var = True

while True:
    input_var = input("Provide a roman numeral to be translated: ")
    
    if first_var == True:
        if len(input_var) > 1:
            if input_var.get[1] > input_var.get[0]:
                total = int(input_var[1]) - int(input_var[0])
            else:
                total = int(input_var[0])

    else:
#       input_var = input("Provide a roman numeral to be translated: ")
        if input_var.lower != "q":
            for rom_numeral in input_var:
                value = numeral_dict.get(rom_numeral)
                if value >= last_value:
                    total = total + value
                else:
                    total = total - value

                last_value = value
        else:
            break

    print(str(total) + "\n")
    total = 0